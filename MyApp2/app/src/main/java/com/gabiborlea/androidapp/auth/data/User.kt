package com.gabiborlea.androidapp.auth.data

data class User(
    val username: String,
    val password: String
)
