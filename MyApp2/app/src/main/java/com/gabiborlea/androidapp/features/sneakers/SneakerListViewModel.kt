package com.gabiborlea.androidapp.features.sneakers

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.gabiborlea.androidapp.core.Result
import com.gabiborlea.androidapp.core.TAG
import com.gabiborlea.androidapp.features.data.Sneaker
import com.gabiborlea.androidapp.features.data.SneakerRepository
import com.gabiborlea.androidapp.features.data.local.SneakerMarketDatabase
import kotlinx.coroutines.launch

class SneakerListViewModel(application: Application) : AndroidViewModel(application) {
    private val mutableLoading = MutableLiveData<Boolean>().apply { value = false }
    private val mutableException = MutableLiveData<Exception>().apply { value = null }

    val sneakers: LiveData<List<Sneaker>>
    val loading: LiveData<Boolean> = mutableLoading
    val loadingError: LiveData<Exception> = mutableException

    val sneakerRepository: SneakerRepository

    init {
        val sneakerDao = SneakerMarketDatabase.getDatabase(application, viewModelScope).sneakerDao()
        sneakerRepository = SneakerRepository(sneakerDao)
        sneakers = sneakerRepository.sneakers
    }

    fun refresh() {
        viewModelScope.launch {
            Log.v(TAG, "refresh...");
            mutableLoading.value = true
            mutableException.value = null
            when (val result = sneakerRepository.refresh()) {
                is Result.Success -> {
                    Log.d(TAG, "refresh succeeded");
                }
                is Result.Error -> {
                    Log.w(TAG, "refresh failed", result.exception);
                    mutableException.value = result.exception
                }
            }
            mutableLoading.value = false
        }
    }
}