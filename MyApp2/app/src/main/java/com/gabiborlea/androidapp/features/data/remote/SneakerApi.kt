package com.gabiborlea.androidapp.features.data.remote

import com.gabiborlea.androidapp.core.Api
import com.gabiborlea.androidapp.features.data.Sneaker
import retrofit2.http.*

object SneakerApi {
    interface Service {
        @GET("/api/sneaker")
        suspend fun find(): List<Sneaker>

        @GET("/api/sneaker/{id}")
        suspend fun read(@Path("id") sneakerId: String): Sneaker;

        @Headers("Content-Type: application/json")
        @POST("/api/sneaker")
        suspend fun create(@Body sneaker: Sneaker): Sneaker

        @Headers("Content-Type: application/json")
        @PUT("/api/sneaker/{id}")
        suspend fun update(@Path("id") sneakerId: String, @Body sneaker: Sneaker): Sneaker
    }

    val service: Service = Api.retrofit.create(Service::class.java)
}