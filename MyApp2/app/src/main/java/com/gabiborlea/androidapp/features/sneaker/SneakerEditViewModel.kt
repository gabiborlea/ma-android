package com.gabiborlea.androidapp.features.sneaker

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.gabiborlea.androidapp.core.Result
import com.gabiborlea.androidapp.core.TAG
import com.gabiborlea.androidapp.features.data.Sneaker
import com.gabiborlea.androidapp.features.data.SneakerRepository
import com.gabiborlea.androidapp.features.data.local.SneakerMarketDatabase
import kotlinx.coroutines.launch

class SneakerEditViewModel(application: Application) : AndroidViewModel(application) {
    private val mutableFetching = MutableLiveData<Boolean>().apply { value = false }
    private val mutableCompleted = MutableLiveData<Boolean>().apply { value = false }
    private val mutableException = MutableLiveData<Exception>().apply { value = null }

    val fetching: LiveData<Boolean> = mutableFetching
    val fetchingError: LiveData<Exception> = mutableException
    val completed: LiveData<Boolean> = mutableCompleted

    val sneakerRepository: SneakerRepository

    init {
        val sneakerDao = SneakerMarketDatabase.getDatabase(application, viewModelScope).sneakerDao()
        sneakerRepository = SneakerRepository(sneakerDao)
    }

    fun getSneakerById(sneakerId: String): LiveData<Sneaker> {
        Log.v(TAG, "getSneakerById...")
        return sneakerRepository.getById(sneakerId)
    }

    fun saveOrUpdateSneaker(sneaker: Sneaker) {
        viewModelScope.launch {
            Log.v(TAG, "saveOrUpdateSneaker...");
            mutableFetching.value = true
            mutableException.value = null
            val result: Result<Sneaker>
            if (sneaker._id.isNotEmpty()) {
                result = sneakerRepository.update(sneaker)
            } else {
                result = sneakerRepository.save(sneaker)
            }
            when(result) {
                is Result.Success -> {
                    Log.d(TAG, "saveOrUpdateSneaker succeeded");
                }
                is Result.Error -> {
                    Log.w(TAG, "saveOrUpdateSneaker failed", result.exception);
                    mutableException.value = result.exception
                }
            }
            mutableCompleted.value = true
            mutableFetching.value = false
        }
    }
}