package com.gabiborlea.androidapp.features.sneaker

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.gabiborlea.androidapp.core.TAG
import com.gabiborlea.androidapp.databinding.FragmentSneakerEditBinding
import com.gabiborlea.androidapp.features.data.Sneaker

class SneakerEditFragment : Fragment() {
    companion object {
        const val SNEAKER_ID = "SNEAKER_ID"
    }

    private lateinit var viewModel: SneakerEditViewModel
    private var sneakerId: String? = null
    private var sneaker: Sneaker? = null

    private var _binding: FragmentSneakerEditBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i(TAG, "onCreateView")
        arguments?.let {
            if (it.containsKey(SNEAKER_ID)) {
                sneakerId = it.getString(SNEAKER_ID).toString()
            }
        }
        _binding = FragmentSneakerEditBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(TAG, "onViewCreated")
        setupViewModel()
        binding.fab.setOnClickListener {
            Log.v(TAG, "save sneaker " + sneaker.toString())
            val i = sneaker
            if (i != null) {
                i.model = binding.model.text.toString()
                i.brand = binding.brand.text.toString()
                i.color = binding.color.text.toString()
                i.model = binding.model.text.toString()
                i.size = binding.size.text.toString().toInt()
                i.price = binding.price.text.toString().toInt()
                viewModel.saveOrUpdateSneaker(i)
            }
        }
        binding.model.setText(sneaker?.model)
        binding.brand.setText(sneaker?.brand)
        binding.color.setText(sneaker?.color)
        binding.size.setText(sneaker?.size.toString())
        binding.price.setText(sneaker?.price.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        Log.i(TAG, "onDestroyView")
    }

    private fun setupViewModel() {
        viewModel = ViewModelProvider(this).get(SneakerEditViewModel::class.java)
        viewModel.fetching.observe(viewLifecycleOwner, { fetching ->
            Log.v(TAG, "update fetching")
            binding.progress.visibility = if (fetching) View.VISIBLE else View.GONE
        })
        viewModel.fetchingError.observe(viewLifecycleOwner, { exception ->
            if (exception != null) {
                Log.v(TAG, "update fetching error")
                val message = "Fetching exception ${exception.message}"
                val parentActivity = activity?.parent
                if (parentActivity != null) {
                    Toast.makeText(parentActivity, message, Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel.completed.observe(viewLifecycleOwner, { completed ->
            if (completed) {
                Log.v(TAG, "completed, navigate back")
                findNavController().navigateUp()
            }
        })
        val id = sneakerId
        if (id == null) {
            sneaker = Sneaker("", "", "", "", 0, 0)
        } else {
            viewModel.getSneakerById(id).observe(viewLifecycleOwner, { snk ->
                Log.v(TAG, "update sneakers")
                if (snk != null) {
                    sneaker = snk
                    binding.model.setText(sneaker?.model)
                    binding.brand.setText(sneaker?.brand)
                    binding.color.setText(sneaker?.color)
                    binding.size.setText(sneaker?.size.toString())
                    binding.price.setText(sneaker?.price.toString())
                }
            })
        }
    }
}
