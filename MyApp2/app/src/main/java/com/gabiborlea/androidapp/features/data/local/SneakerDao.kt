package com.gabiborlea.androidapp.features.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.gabiborlea.androidapp.features.data.Sneaker

@Dao
interface SneakerDao {
    @Query("SELECT * from sneakers ORDER BY model ASC")
    fun getAll(): LiveData<List<Sneaker>>

    @Query("SELECT * FROM sneakers WHERE _id=:id ")
    fun getById(id: String): LiveData<Sneaker>

    @Query("SELECT * FROM sneakers WHERE _id=:id ")
    fun getBySneakerId(id: String): Sneaker

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(sneaker: Sneaker)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(sneaker: Sneaker)

    @Query("DELETE FROM sneakers WHERE _id=:id")
    fun deleteById(id:String?)

    @Query("DELETE FROM sneakers")
    suspend fun deleteAll()

    @Query("SELECT COUNT(*) FROM sneakers")
    fun getSize(): Int
}