package com.gabiborlea.androidapp.features.sneakers

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.gabiborlea.androidapp.R
import com.gabiborlea.androidapp.auth.data.AuthRepository
import com.gabiborlea.androidapp.core.TAG
import com.gabiborlea.androidapp.databinding.FragmentSneakerListBinding
import android.view.animation.LinearInterpolator

import android.view.animation.Animation
import com.gabiborlea.androidapp.Sensors


class SneakerListFragment : Fragment() {
    private var _binding: FragmentSneakerListBinding? = null
    private lateinit var sneakerListAdapter: SneakerListAdapter
    private lateinit var sneakersModel: SneakerListViewModel
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i(TAG, "onCreateView")
        _binding = FragmentSneakerListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(TAG, "onViewCreated")
        if (!AuthRepository.isLoggedIn) {
            findNavController().navigate(R.id.FragmentLogin)
            return;
        }
        setupSneakerList()
        binding.fab.setOnClickListener {
            Log.v(TAG, "add new sneaker")
            findNavController().navigate(R.id.SneakerEditFragment)

        }
        binding.sensors.setOnClickListener {
            Log.v(TAG, "sensors")
            val intent = Intent(context, Sensors::class.java)
            startActivity(intent)
        }
        rotateButtonAnimation()
    }

    private fun rotateButtonAnimation() {
        val objectAnimator = ObjectAnimator.ofFloat(binding.fab, View.ROTATION, 0.0f, 360.0f)

        objectAnimator.duration = 2000
        objectAnimator.repeatCount = Animation.INFINITE
        objectAnimator.interpolator = LinearInterpolator()
        objectAnimator.start()
    }

    private fun setupSneakerList() {
        sneakerListAdapter = SneakerListAdapter(this)
        binding.sneakerList.adapter = sneakerListAdapter
        sneakersModel = ViewModelProvider(this).get(SneakerListViewModel::class.java)
        sneakersModel.sneakers.observe(viewLifecycleOwner, { value ->
            Log.i(TAG, "update sneakers")
            sneakerListAdapter.sneakers = value
        })
        sneakersModel.loading.observe(viewLifecycleOwner, { loading ->
            Log.i(TAG, "update loading")
            binding.progress.visibility = if (loading) View.VISIBLE else View.GONE
        })
        sneakersModel.loadingError.observe(viewLifecycleOwner, { exception ->
            if (exception != null) {
                Log.i(TAG, "update loading error")
                val message = "Loading exception ${exception.message}"
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            }
        })
        sneakersModel.refresh()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.i(TAG, "onDestroyView")
        _binding = null
    }
}