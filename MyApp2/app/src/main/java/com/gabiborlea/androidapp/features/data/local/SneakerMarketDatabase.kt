package com.gabiborlea.androidapp.features.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.gabiborlea.androidapp.features.data.Sneaker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities = [Sneaker::class], version = 1)
abstract class SneakerMarketDatabase : RoomDatabase() {

    abstract fun sneakerDao(): SneakerDao

    companion object {
        @Volatile
        private var INSTANCE: SneakerMarketDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): SneakerMarketDatabase {
            val inst = INSTANCE
            if (inst != null) {
                return inst
            }
            val instance =
                Room.databaseBuilder(
                    context.applicationContext,
                    SneakerMarketDatabase::class.java,
                    "todo_db"
                )
                    .addCallback(WordDatabaseCallback(scope))
                    .build()
            INSTANCE = instance
            return instance
        }

        private class WordDatabaseCallback(private val scope: CoroutineScope) :
            RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.sneakerDao())
                    }
                }
            }
        }

        suspend fun populateDatabase(sneakerDao: SneakerDao) {
//            sneakerDao.deleteAll()
//            val sneaker = Sneaker("1", "Hello")
//            sneakerDao.insert(sneaker)
        }
    }

}
