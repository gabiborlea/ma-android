package com.gabiborlea.androidapp.features.sneakers

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.gabiborlea.androidapp.R
import com.gabiborlea.androidapp.core.TAG
import com.gabiborlea.androidapp.features.data.Sneaker
import com.gabiborlea.androidapp.features.sneaker.SneakerEditFragment

class SneakerListAdapter(
    private val fragment: Fragment,
) : RecyclerView.Adapter<SneakerListAdapter.ViewHolder>() {

    var sneakers = emptyList<Sneaker>()
        set(value) {
            field = value
            notifyDataSetChanged();
        }

    private var onItemClick: View.OnClickListener = View.OnClickListener { view ->
        val sneaker = view.tag as Sneaker
        fragment.findNavController().navigate(R.id.action_SneakerListFragment_to_SneakerEditFragment, Bundle().apply {
            putString(SneakerEditFragment.SNEAKER_ID, sneaker._id)
        })
    };

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_sneaker, parent, false)
        Log.v(TAG, "onCreateViewHolder")
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.v(TAG, "onBindViewHolder $position")
        val sneaker = sneakers[position]
        holder.textView.text = "${sneaker.model} ${sneaker.brand} ${sneaker.price}";
        holder.itemView.tag = sneaker
        holder.itemView.setOnClickListener(onItemClick)
    }

    override fun getItemCount() = sneakers.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView

        init {
            textView = view.findViewById(R.id.v_model)
        }
    }
}
