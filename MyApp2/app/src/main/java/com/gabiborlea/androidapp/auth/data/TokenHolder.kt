package com.gabiborlea.androidapp.auth.data

data class TokenHolder(
    val token: String
)
