package com.gabiborlea.androidapp.features.data

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.gabiborlea.androidapp.features.data.local.SneakerMarketDatabase
import com.gabiborlea.androidapp.features.data.remote.SneakerApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UpdateWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {
    override fun doWork(): Result {
        Log.d("UpdateWorker", "Started")
        var sneakerId = inputData.getString("sneakerId");
        val sneakerDao =
            SneakerMarketDatabase.getDatabase(applicationContext, GlobalScope).sneakerDao()


        val sneaker = sneakerId?.let { sneakerDao.getBySneakerId(it) };
        Log.d("UpdateWorker", "Returned item $sneaker")
        if (sneaker != null) {
            GlobalScope.launch(Dispatchers.Main) {
                SneakerApi.service.update(sneaker._id, sneaker)
            }
            Log.d("UpdateWorker", "Updated $sneaker")
            return Result.success();
        }
        return Result.failure();
    }
}