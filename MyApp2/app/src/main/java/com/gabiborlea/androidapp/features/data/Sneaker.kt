package com.gabiborlea.androidapp.features.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "sneakers")
data class Sneaker(
    @PrimaryKey @ColumnInfo(name = "_id") var _id: String,
    @ColumnInfo(name = "brand") var brand: String,
    @ColumnInfo(name = "model") var model: String,
    @ColumnInfo(name = "color") var color: String,
    @ColumnInfo(name = "size") var size: Int,
    @ColumnInfo(name = "price") var price: Int
) {
    override fun toString(): String = "$brand $model $size";
}
