package com.gabiborlea.androidapp.features.data

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import com.gabiborlea.androidapp.core.Result
import com.gabiborlea.androidapp.features.data.local.SneakerDao
import com.gabiborlea.androidapp.features.data.remote.SneakerApi
import androidx.work.*

class SneakerRepository(private val sneakerDao: SneakerDao) {

    val sneakers = sneakerDao.getAll()

    suspend fun refresh(): Result<Boolean> {
        try {

            val sneakers = SneakerApi.service.find()
            sneakerDao.deleteAll()
            for (sneaker in sneakers) {
                sneakerDao.insert(sneaker)
            }
            return Result.Success(true)
        } catch(e: Exception) {
            return Result.Error(e)
        }
    }

    fun getById(sneakerId: String): LiveData<Sneaker> {
        return sneakerDao.getById(sneakerId)
    }

    suspend fun save(sneaker: Sneaker): Result<Sneaker> {
        try {
            val createdSneaker = SneakerApi.service.create(sneaker)
            sneakerDao.insert(createdSneaker)
            return Result.Success(createdSneaker)
        } catch(e: Exception) {
            Log.d("save","failed to save on server")
            sneaker._id = (1..10)
                .map { (('A'..'Z') + ('a'..'z') + ('0'..'9')).random() }
                .joinToString("")
            sneakerDao.insert(sneaker)
            saveInBackground(sneaker._id)
            Log.d("save","send to be saved on server")
            return Result.Error(e)
        }
    }

    suspend fun update(sneaker: Sneaker): Result<Sneaker> {
        try {
            val updatedSneaker = SneakerApi.service.update(sneaker._id, sneaker)
            sneakerDao.update(updatedSneaker)
            return Result.Success(updatedSneaker)
        } catch(e: Exception) {
            Log.d("update","failed to edit on server")
            sneakerDao.update(sneaker)
            updateInBackground(sneaker._id)
            Log.d("update","send to be saved on server")
            return Result.Error(e)
        }
    }

    @SuppressLint("RestrictedApi")
    private fun saveInBackground(sneakerId: String) {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val inputData = Data.Builder()
            .put("sneakerId", sneakerId)
            .build()
        val myWork = OneTimeWorkRequest.Builder(SaveWorker::class.java)
            .setConstraints(constraints)
            .setInputData(inputData)
            .build()
        WorkManager.getInstance().enqueue(myWork);
    }

    @SuppressLint("RestrictedApi")
    private fun updateInBackground(sneakerId: String) {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val inputData = Data.Builder()
            .put("sneakerId", sneakerId)
            .build()
        val myWork = OneTimeWorkRequest.Builder(UpdateWorker::class.java)
            .setConstraints(constraints)
            .setInputData(inputData)
            .build()
        WorkManager.getInstance().enqueue(myWork);
    }

}