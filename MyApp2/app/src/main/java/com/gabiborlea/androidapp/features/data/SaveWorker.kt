package com.gabiborlea.androidapp.features.data

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.gabiborlea.androidapp.features.data.local.SneakerMarketDatabase
import com.gabiborlea.androidapp.features.data.remote.SneakerApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SaveWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {
    override fun doWork(): Result {
        Log.d("SaveWorker", "Started")
        var sneakerId = inputData.getString("sneakerId");
        val sneakerDao =
            SneakerMarketDatabase.getDatabase(applicationContext, GlobalScope).sneakerDao()


        val sneaker = sneakerId?.let { sneakerDao.getBySneakerId(it) };
        Log.d("SaveWorker", "Returned item $sneaker")
        if (sneaker != null) {
            sneakerDao.deleteById(sneakerId)
            GlobalScope.launch(Dispatchers.Main) {
                val createdItem = SneakerApi.service.create(sneaker)

                sneakerDao.insert(createdItem)
            }
            Log.d("SaveWorker", "Saved $sneaker")
            return Result.success();
        }
        return Result.failure();
    }
}